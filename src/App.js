
// import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import MyNavbar from "./Components/MyNavbar";
import {BrowserRouter, Route , Routes} from "react-router-dom";
import HomePage from "./pages/HomePage";
import ServicePage from "./pages/ServicePage";
import NotFound from "./pages/NotFound";
import MenuPage from "./pages/MenuPage";
import ContactPage from "./pages/ContactPage";
import Myfooter from "./Components/Myfooter";
import ProductDetail from "./pages/ProductDetail";
import NotFoundPage from "./pages/NotFoundPage";

function App() {
  return (
   
      <BrowserRouter>
      <MyNavbar />
      <Routes>
           <Route path="/" element={<HomePage />} />
           <Route path="/service" element={<ServicePage />} />
           <Route path="/menu"  element={<MenuPage />}/>
           <Route path="/menu/:id"  element={<ProductDetail/>}/>
           <Route path="/contact"  element={<ContactPage />}/>
           <Route path="*" element={<NotFoundPage />} />
          
        </Routes>
        <Myfooter />
        </BrowserRouter>
    
  );
}

export default App;
