import React from 'react'
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';
import "../styles/MyFooter.css"
const Myfooter = () => {
  return (
    <div bgColor='light' className='text-center text-lg-start text-muted'>
      <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='me-5 d-none d-lg-block'>
          <span>Get connected with us on social networks:</span>
        </div>

        <div>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="facebook-f" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="twitter" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="google" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="instagram" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="linkedin" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="github" />
          </a>
        </div>
      </section>

      <section className=''>
        <MDBContainer className='text-center text-md-start mt-5'>
          <MDBRow className='mt-3'>
            <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <MDBIcon icon="gem" className="me-3" />
                <img className='w-50' src="https://i.pinimg.com/564x/a2/67/76/a26776ac13bf650b0b65e3dcc642ccee.jpg" alt="" />
              </h6>
             
            </MDBCol>

            <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Luxcy</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Porsche 
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  lambogini
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Mustand
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  ferrairi
                </a>
              </p>
            </MDBCol>

            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>JDM </h6>
              <p>
                <a href='#!' className='text-reset'>
                  GTR
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  RX7
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  SUPRA
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                 Porsche
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Honda
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Suboru
                </a>
              </p>
            </MDBCol>
            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Sports </h6>
              
              <p>
                <a href='#!' className='text-reset'>
                  Ford
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Bugatti
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                 Range Rover
                </a>
              </p>
              <a href='#!' className='text-reset'>
              Andrew Robertson on X
                </a>
            
            </MDBCol>


            <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                <MDBIcon icon="home" className="me-2 " />
                Address:#829, Street 240, Phnom Penh, Cambodia
              </p>
              <p>
                <MDBIcon icon="envelope" className="me-3" />
                Email:carjdm@gmail.com
              </p>
              <p>
                <MDBIcon icon="phone" className="me-3" /> Phone Number:023 974 546
              </p>
             
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
      © 2023 All Rights Reserved. Design by{" "}
        <a className='text-reset fw-bold' href='https://mdbootstrap.com/'>
          Heng chanbo
        </a>
      </div>
    </div>
  )
}

export default Myfooter
