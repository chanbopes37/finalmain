import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faPaperPlane } from "@fortawesome/free-solid-svg-icons";

import "../styles/HomePage.css"

const HomePage = () => {

    return (
        <div className="container">
            <div className="d-flex header-section flex-column flex-md-row  mt-4 ">
                <div className="container pt-4   order-2 order-md-1">
                    <h1 className="display-2 header-title">
                        {" "}
                        Welcome to the Cars sport
                    </h1>
                    <p> We are a team of passionate people whose goal is to improve everyone's life through disruptive products. We build great products to solve your business problems.</p>
                    <div className="d-flex pt-5">
                        <button className="btn btn-success px-4 py-2">
                            
                            <strong> Go to Menu </strong>
                        </button>
                      
                    </div>
                </div>

                <div className="order-md-2 logo-img">
                    <img
                        className="img-fluid "
                        src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUUFBcVFBQXFxcZGRocGRoaGCIhIxocGRoZGRkcICAiICwjHRwoIhoaJDUkKC0vMjIyGiI4PTgxPCwxMi8BCwsLDw4PHBERHTEoICgvMTExMTExMTExMTEzMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAABAgMEBQYHAAj/xABNEAACAQIDBAYGBAwDBwMFAAABAhEAAwQSIQUxQVEGEyJhcYEHMpGhwdFCUpKxFBUjQ1NicoLC0uHwM0SyFiRzg6Li8VR0wxc0VZOz/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAJREAAgICAgICAgMBAAAAAAAAAAECERIhAzFBURMiYYEykfAE/9oADAMBAAIRAxEAPwDGa6urqALvsS7+SXWAMs+AUCf750htJjKtGYAqSN5K7tBx4H30xwW2ba2wjK5OXKYAjlp2prm2paYSWcEADVeQidGrOnZvcWuyxneh+tr/AKY8KdSMskz5cJ/8VD4HaFtoGcMQPAjjpx3/AN6VJX3AtwNeRPA8KXkda0I43SxenSbbD2KQffNI424t5suVgyrmBB0OXKCD4hh7KUxLk2bpPG25HEaL304fFKtu27rkMLrG8RlOsboI9lNETdkJfyNbh0zqvqx6w8DvieFMjgMM4JR2QASQQxI90T51N7Q2f2y1tiATJjXUDx0GntPnT/A2kCaoGkkFiN+UB14ctfEU1ZLoqoUZGKsXVQDDDtAcCrfAzUYMdl0USOE76u925afrBbtwVAnQCQwnhv151nZpxe2gl0iXwG3Gtvnyg6ERMb4+VW/ot6Q0suwvWjkfL2kMlYneDEjXhyrOK6qeyFo9L7P2naxNvrLNxbiniDu7iN4PcaFF0rzps3aV3DuHs3GtsOKnf3EbiO41pXR30jo8JiwEb9IvqnxG9fu8KhxLTL8p1HhRy9MRilcBlYMpEhlMg94IoDifhSKoxXat5xjsSEYjPiLikA+sDdYQeY3VvHQ7Y9u5g1622M+Zg0iCMrHLPlGnhWB41/8Afbj6hvwlipnj1s1rexdus8lbhW7lgszMc4BaDA7OYF5J0mPGtEZN0WHpdsK0uEMLqrLkCrGrMAx04RJPhWX9N7bW8M1tgQVv2tD/AMO6flWmY4XbhINy2QD2ZuOrTLCdPo6kR3DWs16f5msdY4gviE08Ldyh9BF7/soaGtD6M7a6621q4fyqKdfrqBv8RxrOkNS2xGQXMzOyssFMqZp3gyJGm7x1qZRyRcJuLNMe3KATEgeRgVKX7gXAqNCVvNH2R86hdm45LyAgyV0YFYgxvjgD86eu5KBJ0kn2xr7q53p0dX8kmG6bgm1YDLp+T1nf2LmkRpS+EshbeYbzbM+SGkOkeKF3C4VssEm3JDzrkuBgRGntqp7I6SXLuLeypi0tu94nJafXwmt5fxOPybPs1fyNoRplt/6RVD6dPG0cMnC5dsBvAOxEeYpDod0xuOiWsVlRurV7dxWADoInMAeyQCDwnXQRTXbN4X9qYUo5uDrrGubdBYmNZA48DrWCe8ToS1kajicPYW51txgtxkySzxKgzETG+ksYtsKChBLaiGmRz7xu9tUfpNbzYu5mLEqQFljoAo3a7pmnewsW1y6wzNktotsKTMHKrNHsnzqoTedJGcoLG7J40BoWos10GQBohFHNFNACZFBRzRaAPNFCqkmBXDd5/KnWBtBn7RgDly3HiKTY0guIs5cscV10O/4/0prU6tmSsjSH/wBLxwilMPh8KhnFM/8AwrQGc6GMznsoCRG5m13DfSTG0V6nuCxd1WAQs36upB8qvOztq2VQNYwWCtDeDdRsRc3fWZgAfKB3bqVv9L8aVypeFocRZtJbnzClh5Gq7JuiPwdvFXrbWnwt63mWFc23ySdNSR2R3zFWPB7Ju3rYtnDXSUhSckq0CCVZZgHvjcKqd3G3mJLYi+xO+b1z+agfH3CMpuXCORuMR7JpYIpzb7Lvg+iGKTNKBO1IL3EA1MmRM84Hf3U8GwAhBfF4O2O1IN+N5B3e2s1W0Xnsg8yY+NHTCEHcoH991PEWRefxRhRnDbUwCqdBlcFgO85xmquDoJgOO3LHlan/AOWoc2m3iI8f6Uk5Zd9CilsGywDoVsoettkH9nDn+Y0YdEtjDfta4fCw38pqsm4edAXNOhFn/wBltif/AJW752G/kordENkH1dsZf2rJ/pVXLmk2c0AaN0Y6K4dLgGG27acH81kXtfum79wmrvjNgvbEm4r7t4yyRv3kj315+Z2BmCCDIO4g8weBrSfR5t25ird3A3WLsqi5YzMZJQ9u2G3iRu5a1LimNSaKdtHDPbxLrcDW2BuOAyaE5+ye9TO8GrD0GVhe1CXSbeaDqo3jdzBmpzbezM9rOhe7bjMbbksygj17bmbgI0JQ5pAMAnsmpC/cssWtnIcqw0AyGBM8QQRuYSCBod9JJxaKbTi/Zpd/FQCjWLStz6vUAjhy51SfSHb/AN1tBR2mvgAcyUcD76q+0OlmMR4W7pAOqIf4aHZ22cRjMThrV586i+jBQoGs/qjXSrb0ZxjTuyJx+zrthyl22UYBSdAQA4BXUaazQ4G/kcNE8PbW+7Rw1pxdS5bV1Y2FYMN7JmiQf2R7Kw3pDkXG3+rQIgusVUCAomQAOAqE7KaoWwm13t3zdWY3Ms+sOI+INaDZxtu4A1tmMgHjpPMc6zKyqs+hzEnsrHEnSZ099TmzsX1dyCBlOhLRAJ+lMyIOsilKClsuPI46F8PcY9knsBpEE6nORPspv0Z2e9rGXCR2DZxJVuBHVtp3ESJFKuVy9l1aZ+88wAfLSlMLcu2mJVVIuoVYApMMnViBm39vx38JqmrRmnsb9FcAxUXGKqMvZUkamMoMeBOvfVqwWzQLmCuKACuMthj9aYyndpAMaVSRtD8FAsoiEqAHZkE5/pDedAdJ4xNX3o5ijds4NyO0cZa0H6rAT7qxcWpNs0Ur0NOmWNupj8SA5K51hDDAfk7e4HVZneI40foXin63KxYJBcuSIBK5dSeJ0A37qadOGnaGJifXXnwt2x8DUt0EuK1u6twg5SuRTI0yEzpodePCisZWVdxSLQNo2m9W4zeC8Tw9Xf3UezeDkgM4K7wywfESuo7xXXlGZywlSbZEZp0Mazx09lKbRVy9o28sAnPKwYytx5TGnOtIysylGgSv6x93yohHefd8qUNENWIKR3n3fKi5T9Y+75Uc0FAHmsDTz+VSWzNGaFJJKqAoJJLaAADUknhUdHZEDifhVkXELg7ZFsg4lx23/QAgjq0P6YgnM49Wco7WYiWUtCe2Lws/k1M3vpkRFqRBQETNzXtGYXcJOogLCZmCzEnfy5mhY66DSneGtR220EHL38JpoGThAAAGgAgUQtSSXDlE74E0U3KsgOzUWaIXopegBwlwruNCb7c6bZ6EPQAr1rc6LdmASd//AI+FHxKwR4Unfbsr4H7zSATNFY0VnoC1AAzRQ8Ga59BJ8qblqAHD3QTJNOdh7UOGxdm+h9S4pPeJ7Q8xI86i2aky1AHoPFqLd+4i+q0Xbf7F2SfY4fwBWq7tXZKO2QwFcnqmj/DuHtMhj808SB9FgYMlQJTZN4X8Fs6+xacjWHIieKoTMA9u3bG8euafrshrrmySsxLQ4JTcQSASVYGIniO6gDKm6E377uUa2uRsjI7QVIgwe4gyCNCCDxqQ2N0Cxdi/avJdtZrbhx2hvB7zFaNtb0fjEBS1/JcCgO62/WgGNM+kEkjUwCw41X8f6LHt27rpjGcrbZkTq8pZwCQC2c6HdurNqT8lqUfRM4tr7XCxCDObR0uKYKz1jRJ56DSs72p0TxrX71wW1cXHJXK6EnXQQW0kVBpfujdcuDwuMPjUhsnbF23ftB7lxlLgQ1wsNeOpPCaSg4q7spSTZXcXhXtXGt3EZHU6qREf076Cy8MNAa0rpzhLWJV2iL1u2rI31gRqh7swPhJOms5ejgGZ3Gqi7VomXZb8VhWs5kbU2yZgb9Z08jUhjrrXOoYW7gUQJOXechA9adchpDH7Tt3LrXA4C3CSs6T2I4+FSmLYG3aykEG7b3H9VjTZJnm0Axv3SQY6x98SO2dN9XPobji97B21JyJi03/WKsT5fEVU8bhnu4m6ttWZususQBOVVcksY4Aak1Zeg9jJisKJH/3SefY++pl0Wuy09JsVbXH4gOYhlO7j1afM0x2BiLdwmxcLgam2yASGgwJMRMiBupt01U/h+J1Grj3JbFV57b23DEsoeCMukgADhWeNyL6jZqWHxAuMEdXV1NtZIVcxLEA84JkRzqevWiu/75rHMNtW5cxCXGuuClxSOsfQZdRJ5aDWr5iekmGvZbSXVa+SoFtbhBLNEgOBB3mtIRxIk72T7UU0KIFAA4czPvoDVkBTQUJoKAPPdn8kin86e0g+opiH/bP0eQ7X1aYFtddwqaubJuktmK521YknUnfuFKWOit1tzT4CfjSoZE4cjMCQCvEGpW4QdNIqX2b0Fu3Gyqtw8zuH3VeNl+jvB2Rmxd1T+q1wwO6JE00qB7Mte4Bxgd9F60cwJEiWAkcxO/ca3LDpsfD/AOHZtZhxWySftFfjS/8AtNg83ZwzT9Y20H3Et7qexGEYew9wTbRmAMSqkieUjSnA2RiDusXPs1uj9KFA/JpaA73b7ggppc6U3uDWR4K5++jYaMYGw8Wd2GunwSjLsbGIwP4JiCRrpZdh7QpFa8238Q3+YUeCf9lJNtC82/FP5Mw+4UUwMrvbOxrmTgsTP/t7n8lIHB4rMU/BbuZQCV/B2zKGmCRkkAwYJ3wa1Vndt99j4u5pI4YE6mTToRlzWsSPzF1fG1l+AppiMTfVioW5pyB5eFa7+CLyp1s/Yj3mheyOLRupUMxK+154BS4e4od/spveV1HatuvMlT8a9NYTo11eq33Dcx/Umng2dfG7GP52kPwoEeU1ObQR5kD7zRGfwr1i2FxH6e237dn5OKTOGux+Ut2HXiFUgn7UikMoXoxwZv7IyExlvsyH/h5Lg/6lq6WMbhbIuXQbfWN2nVerVySJIklS2vM06xuLS3YJ/wAMGVUAAEEyDAiNNTu4VRtn9E7Atq+UGQSS2Qk66kkrPj3mgCZuekG2PzLA97D5VF4vpq9zRFmZgKJJgEwOZgHSpK10etqpTqzHEZF94y04XZSdkEMcpDLIEqRoCNNCNwpUyrSMIu4vOzNHrMW+0Z+NIXr0Qw3qQR93xrSemvQYuHv4XVxLPZ+tOrFBwaZJXcSTEHfmFu29zsopYzujjO7xpslGu4fDC66yJF2xcTUSFNt0uIzd03AI41Tdr7TuYRxaNu2yBRkNxFLZSNAxIMsNVJ4lGq24NxbfBMxEW7zBpO9bltbBA5yboMclNNsZtFRcuDU5blwDydtfPU+JNefH/p+PjTq/B2fDnNropr7fttka7hsM6A9pVUBsp9bcoIMbiDSNzZa2bhKGAT+TbOBK3EJtnVvWgggjlUt0pxdp7DZl7YjI2kgz7YPGkuj99xgjetw7WrgV0aToutu4ACNQpZZ4BO+uvi5fkjlVHPycfxyq7IzZ22Xw2IFwZrrBLlsy4J/KqbZ1gy0kHU8KebExl3D3rVy6p/I31LAEa5VVSN0yBu1PzUsWUxvWZbaJiLZW6uSR1iZu2CJgsCZn9Ya6GkcbiFYOTIzXiwzaHL2B7eB5HSqlSFHZI7Sx/wCGYu/ctAsGbMBlIOWQoMH9mu2mpdbVtWVWtqQwZoggrpUbsTGKl+3bbRWLHNPKdDzGlN9uWzcvXHQyHK5dOCiNOe+iO5a9Dd40/YK7OW86q50kjRgOB47uG+rd0a6BEXLWMW+AqPOTJm9TsgZw0Gd+6q50TYlrqXiAkLLCJBBLEKRz0q9J0iuYZQlq2htAnICSwbOSCdO0CCNDPGm3siizYZ2ZZeJk/RI3GOJNGJo64hbhZgVns+qdIKKd2/fNEusFBJ9nEyY075Ip5KrCgKCKbYW5dLKtyyy5iYPAAc++akjhDzHtqYSUlaBqjPUwaTJEk7ydfeacLZA3VLLg076PbwqcBW1EkbnaIzNHKTHso1rDFvVQt4Cal0sqNyinAuN9Y+2gCNTYl1terI8SB7iZo42Lc3ZVHiaf5jzPtoIoARt7Ab6T2x5n5U5t7Btj1rknugfOhsYdnMIpJ7qmMLsA77jR3L86QDO3sqyvBT4sfnFHXBqvqWw3KEn31PWNn209VBPM6n308pBZT22Ldclimp4kgeQE6ClF6M3D6zIPafhVsrqLAradFl+k/sX5mpzB4RLShUED76cV1AHUilzN6u7nz8OfjTbN1rMo/wANTDn6zcUHcPpHn2eDAPSQBQAIEVxFN7mJFJnFgbyBQBG9JMEbi2/qo0sI3jT3b5pricULdoPOUJAIAmOCkDhvjzqUxW0FyEgzwHidKgsHtGxeS/ZRkdlhLiSewxmBMDQkDUcQeIqW6KSI+50mtKQBbzLx1Kqf3AQpPiKeJtjMkrYuhJ7UIFXtFVEkACB3yagtmbYNsxbs2l09bIWbvkkk+yBU/itp3mQG3cnmMoB9nAcamyqFrm0Vtutu5buKDuZVmNNN2hX9md5qC21hwtwFAMtxcw8fpeHDwmpK3tFuz1jyOyDmfVW3FtN0kkb91K7VFt7LG4yqAMy3CwAUnmT9E8STu8Ky5Y5xa8l8f1lfgqW0cO6qjKzA5tCjayeUa66jzqUwHRkJ1tzF20CohftkMGkyzHWJEcfrVX+im1nxDnPbVRoMpBJkGePGVImJEE7xWhPtlDbGZpYgggEScpKPOhESN+7TvFckONRl9tVujolNtfXd6syX0jdGEtMl7DIRbcLAVWKHMrNnU6gboK94qH2HhLyBrQS6zYm3cVLdtgozouZGeTBUS0qY0M8AK1bpJeS/gbyJ64RXtoeHVAMFGkaiRwmay9NuXsPctrH5QQQIBILCBEcwY8DFd/HJSick4tSF7FjE7LxWEu4pUKluBBBQ9i6hgRucGDPCONS22tuLhcZiLdi4QjEhQVDLnYKwdSTzJnTUk8hVk2tat4o2muQ1u7btgSBFskOxceL5J7lA0FLbKTZ9vBpbeyj3HVrVxhbCsWC6sWG6dCOMnuqlKLVicWtFQ2Vj7mLN+3ds2rl63aNy3NsKT1Rm4hKwdQ3tAp4t/DsLV249i3nQPbD2nMLO7MGgwRBPMUfZ/QjEFkx1zE5D1rEIgjP1TQpnMAOsCFtBx74px0t2FedGtK9kNaCFbfVwSpPaht2hOvee+i9gl4I3alpERbmHCZibhfLOXMLc27gGpCMD4SppbAbRe3YsM2Qvq0C5l00cDdoYI058aZYTD33wtqxbXM4ulyVaBlE5AzyFjMzDKd+nfVPx+Z75tqgGWEAHEJ2QZ7/lR3objWzRMR00b1DaCm4pAK3cxUnduq9Y/E/k0PVgy1sFp4FwBx3zWDYaQTbK9tG105ZifhVhwuPNtcyJdYtoerZjlZLnWAwQwXhp3TxrOcLVRKUq7NGxeNjG9XlCkunaBIJDom8biROmv9abgtr4hwT+FNoxGra8D8aY4jbuIcZytzNbAIuXbaiNVWc+RYaIieWlPdhbMDWpNp2JJkrbzCYHHjWdY6YSkn0WP8cYf9Pb+2PnRU6R4Mf5m19sViQxdwbnf7Rpb8Z3ea/YT+WuyzGjZ26U4If5q35GfuFJnplgR/mFPgrH+Gs62LttRadbtm28dYesZBIzqqgTB9U9pYjUmZFMtt7SuM6uhFtSqgLbhR2AFk5YljAYk8WaNKLHRqA6aYE7rjHwtXD/AAVK4bbOHYrmuFFMEyIMHuMEedYN+Mr36a59tvnSFy4WJJJJO8kyTSsD1DZ6V7PtrAv2kHfcQT4y1P8AZXSLDYlymHuLcKiWyMrBRuEkMYnhXlvZ+yr17/Cs3LgmOxbZveoNbts7ZH4n2U4XTE3R23GpDlSTB5W0DkcCVJ+lSCiD6YekfEm/cs4IpbtoxU3SodnKmGKggqqyCBoZidJqHfHY66LZbaWJZrgBy236vKWJCr2YknQ6c4qrlOAECNPhVn2dsxrq2jrlDKGK/RMkan6O7j3GkxofpsPENm6zaOIUp6yvinBkTIgmZ0qr7dW/ZIIxd91JjW85IPf2qsWJ6y2oTq8zwsHUjz76rfSXG5lWWX1gWgzEjx/uKlS2ViN8E165uv3+78q/zpS01/NlGJxAPddcR/1U32W4IyyDvgyPL/xUm2EC5XV8xJ3AHTdx3f8AihsFEOdr4/C5Rax18ITEM2eGOZtAwO+CasWA6d49Vh2t3yB9NAhbwZIA81NVvHpcYAW7V1u0CSLbHQBp1jvFPrZurbXq8HiHubiBYeOQklY4c6jOT6ReEUtsuOxelRxoIBa3cAk2zG6YJDDeJ8PCpHqydWme81QOj+ydo/haXbuFuKnbDE5QQGXTSQTqBwrQEwl/grAd5PwBrRX5I0R+3NoHD4e5cmCqsU723L7yKonRjaDC+gGQZlZSzCdVBu5mPF4VivLNxk1YvSGHXDLmYGbtsESd0luXNRVL2cjW7rOEZVXIw1mRbhSQRuBAcR+t4VMikaZs3Z/WNcHWFIY6AgfXI3TwU1I3sJhsMfyl5y0EhczGRryge2oFXQsSUDLwiddQZ5Rv9tMLljk0ewfE1KsbSH1wI0ncWaVykEZdSc2p7W7SonpNj3t4ZhnY9a6oVJmVId2kGQfUX7QqQw7hFILHWeM7wJ3bxVS6Y4sXL9m3OgVmOg3toD4wtUhPokdju2RLy9gsoCwZIhSCSechj5mnXRPGDEYrFoSNXJVSYzAAhlEEQTkB076Y7NTIjWlYkW7rqp4lSZU+YM+2oXo1j+rxF11PrS0jeO0w+4++uNJuU78HS6UY0XLpHtBbdm5kuAZbIAXtSpdxoCT6wB398d5oGHLYrEtc7Cu0ZEAMAwFVV8AIq1dJsDcbCNdvMllSQyW2nPdC6+rvVZI1PMcNazqy7EwCdZ4nXTdpXRwxqLMOSX2RpX4Y5thVHaW5bzy2mV8iFgd6mdYjSSeJiM2ZtFSbklpFu42jEdodWBMgkmW/rUFdx2VJQlMyxCmBmVEzCD3nTwoy7PvPcy2UZiELMFn1dCxMa5RA9orWEatEzd7/ANstGztvtdjDXGm0r2ym45YATdzJEj4zTrpRtlkyK0o2a6DcOufLcMxpuWFESdSOVZ1hbvV3bbknLnR2j9VzPjEGne3dpJeuMUDEZngk8Gd20EDfmncNaUoWxRnRPbN2nktu6aqFXPrMlTIJmNeMb6qz4m5dus3rO4IE8BGkTyURTuwz5f8ADC2iyzm4wZMcz4Cpz8b4YDKmDtqJ9bKuY+caDuFUk0E2nVDNSS6u1lCQsZBcIAYEQxK7xAjKI376Jh8VdtsyKCRvB3QSPHXWdJ5VKYfbFjjaA8FX5ilX2vhRp1Z+yPnR+ia/IOz9t3LaFLidY1xiCT2kgjQMubRfCnWw8S9u2Vc3QcxIFu8yqBA4ARvk+dMTtfD/AKM/ZHzrvxzY+q3s/rUyV9oadE4vRNP/AElrzuH4W6OOhiH/AC1gfvOfgKefjRvq3D5j+ah/Gr/o3+0PnWxmM26FIfzOGH7tz4XBS+E6DZ2Cpawk99pz7Zu0c7SuH82fNxUp0b209u7rbUyI1uR/CaBkxsf0fYe2Ju2cHcb/ANuYHd2rhB9lWOxsSwkZbGHWN2WyoinWGvu4kog8HJ/gFLq2sRujw15HjSEAiQIEAcgI+NMdo7Is4gob6C4EzZVb1ZYAEldxMaa7pPOpKuoAgv8AZDZ534LDHxsofhSidFcAu7BYUf8AIt/y1M11ADBNj4ZfVw9keFpR8KcJhUX1baDwUD4UvXUAFCAbgKNXURnoA53gTVfx3SFkYqqDzmn+OuGDVOvqSzc5qWy4ofXekV87mVfBR8aa3NoXbm+457uFM7qwaYbe2lct3MJhrJym6Wa4wGoRACR3TumpsqqID0k3yLVpSTJuE/ZRh/FTXAuGUEmJW4hA4w4In/8AW3tpD0pGHsJJMK51PPKJ91DsG1nRonS8s9wuQB//AEpPoa7JWxdY20k7lA9mnwoGfvp7h9j3CCzNbtJmMNcuATrJhRLnfyo1yzhbfrXbl1v1FCL7Wlm9gpLobAwGz7t0Hq7ZYDe25R4k6eVUPbysmPyvGhQCOUR95Nae23AFFq2erULmgS2SQWg6chPDU1lnSfEAYwPqQuQnmYM+2qXZMui14HDMGuDRe0PW0EhQdfbVY6J4h7F8lbavcysoDLmyEMkMBuLAyNZrUNv7JtWrauzOy5yGjQuTLDfIHZKif1Z41lOGxy27txgWQTcHZYyQzAhfHQ8p0rngnlL9G8mnGP7JDpKbrq9y8xa4WUHNvga6zqBqIHdVas4fM4XMqTxYwBxp/jdtl7XVBFCkzPHhA5RpPPXwqPvM+YNJzEBpHM8dONb8cZJbMOSSbVEnew3+7sH7LJcYjNoWDImWA3aMgSO7200xOMYyAxH5NB2dJlLYYGN47O48qJfxVy4oDktGWCd8KuUD2U/2dspmUM7C3b5nQkd3OrRLdkZasF8oE8o8yfKpdMHbtrLw7/UXcP2m+AouJuIdLaQBxO803znvpkiruT5bhrp4TwohBoAx767KeVAC1hBOomhxEBtV99Bh0110ocUdxmmA+wuEtOJ6xVPJmil/xda/S2/t/wBags1BNTQ7LD/tpb/RP7RXHptb4WW+0Ko9CFp2IujdN14WT9v+lEHTkgytgfb/AO2qdFBFFgajgfTFetrH4KjeNw/y0XHembGMfyVmzbEagy+uus6d2ndWYxQRQBf39Lm0z9O0P+WK616WtpTrctn/AJaj4VQIo40176Qz0N6OOnpx7PZvIEvKuYEbnAgNpwIkePlV+a6o3msJ9EOEb8ZBhoFtOzd49QD2uPZWi+kfEXLeHPVkjMYJ8jHl8qL0FWyZu9K8GrZTeGbkFY/cKL/tXheFxvsH4isO/A7IUHq3NyTLHuIgg7+fhV4wuEHU2WklmVg075R2T7gPYamM7NJcdFyxfTLCW0LuzhQJJy/1qCuelbZf6S6fC0aqfT3CEYJ2/Y/1rWRVSIao9At6VNlsYJvxz6v+tTm0FwwVHXOQ6B1IiCragya8xgTXoPCEts3ZxP8A6dR5KqAUPoI9hGKnQA7+J76jXt9ZtJm4WrKIP2rhZ288oX21KYZJYUy6Prna7dj/ABbrn91T1SHzW2D51EWXJGeelC7OMUcrS+8tTjoTcbq8SRrlNhz3CS5PlkA/eqM9Id3Njrn6oUe6fjU36OINraAPHBOfNJAqmrRN/Ys21dnRfcM7GW0AAlsvZkbyTCzAU0nZ2UCCerIC6lnJBJ4CBqAeIbLoPVNW1sUtslimYm3baWbKoBBOp48dJqvba6Q2oliCRrp2UXTcBx3cPbUpFWV+9s65mS4ACipde4wI9dkNtARv0zHd9aqD0gecQ/dlH/SKsG1ulgYkJJHEDQE9/PzmqheulmLHeSSfOqRMma10m2rc/FauH7ZOHeYBjrLKEjUcNRu4VlV67nBYmXZiW03zrOnfNWpsabmyFQsJS8LffABuDviDA8DyquJg475p0rsm3VDJbZNPsHhnchEXMaksBscuwDaGfU1zHju3DzM04xwt2hltkZ51gmVjmZ30xBWs28OFLrnufVDaDxEU0xOMe4ZY6cBwHhSLMSZOpO+f612f+4FAATXAmjZjRS55n20AGyHvoMjcqLn5/fRl13LPhNAC2HEHUV2JJ05Urh8I516tvsml3wTka27nkp+VAESRXTTptnXuFq59g/Ku/Fl/9E/spWh0yEC0oAP7FIGjqRxE+dMQoY7qDTu9tHF5P0Y9p+dHF+3+jFFAIyP7NcY5++lzet/UorXk4IKKARMf2aLw30r14nRF86cWsWvFEH7opMaNN9DuNQ45gTDNh2CzxOe2xHjAJjuPKtkx2Ct3ka3cUMrCCP73GvNWzdoC3dW7aUBgREaRu3Eairvtf0j421YtXEayXZZcFJgl3A3MIOUKT4+yVLwU4+S/YPoTh7e6X3xngkA7hMQY5kGnCdG1kahVUQqiTA37zqSSSSTvJNYnc9LW1Duu218LS/EGmtz0n7Vb/Nx4WrY/gqqQsn7Nm6YdE3xWFexaZAxAguSBoZ1gGsy/+jWNUS+Iwqj9pz/8Yqv3+nu0yoJxl3UndlG6OSjvqMxHSnHXPXxl8/8AMYfcaBFxs+jS2navbRtKBvy2naPbFWbbnS3B4fqsIvW5MPbVASkT2VjeQZyhTu41k2Fxt+8wW5iLpSe2WuMQF+lvPKi7Vxq3HdlG9pHcBoB7IpS3oqNLZpB6dYOD2rgJBE5JiRv0NN9n9OMHhrSWk625kUKDkAmBE6kVlsTRwQveaFGgcr7Hm38b1+IuXQCA5kA7wIAE+yrZ6N1zW8dOuXCXY8TBUeZmqGzSZNWboNtJrd57QE9eq2wP1hcRh7QGX96nRN7LP6RNs9TdtIq9o4eydfo+t76zrF417hl2J7uFWX0nYjNj2WZ6u2ls+IBJ/wBVVGKKBtgV1OLVkkgASTwqcwWxNMzyP1RbJ3eXwpiIvZ1m42iIxnyGnGTpU5svB3LbZ3GUiYLyFAgyZHrHuoMRtM2xkt5f2eqgD2n4cKiXus5liSf73cBSAkMbtGXLWycx3vETpqAIBA8f61Hqp5UFDTAHL4e2hgc6mdm9HLlwZrjraU89WI8JEeZ8qm8N0asofXzn9aD7gYPnNQ5xRag2Q+xNm3HBfIhThnRTPeM3D3VKfiy6NyIPC3aH8JqZ6hhoLrAcAFT+U0ORh+cc+OX4KKzc2zRQSRF2cFd4sB5J/CgoRhrk9ptP1brj7iKlZ0ojnSkpDxIxMK8yygrwIdyfeYpVsKp+i3kxH8VLs88/CaKrEVbZKQl+AWuNufEk/eaL+AWv0SfZFOVagmotlUjKZHI+3+ldI5H2/wBKLXV0HOGBHL30Mjl76JXUAHzDkPaaDMPqj3/Oi11ABs4+qPf86Vt3wPzaHxn50hXUASNvarLqqWxHcf5qG9ti4+rBD+7UbXUqQ7Y5/Cz9RB+6K78Mbkn2B8qbV1AWxw2KYx6um7sj5UTr27vsj5UlXUBbFHvMd7E0QGgrqYgZoKMqk7gTS6YC63q2nPgh+VADalbN1kYMpKspBUjeCDII76kE6P4phIstHfA+806s9E8WxAFrUmAC6/A0sl7Hi/REYnENcZrlxizsSWJMkk7yacYHBNcMKPEwTHHhWhbH9EGJeGvXbdtY3DMx/wBIHsNTN/0dYfDKTcxxkjRVsrJjdAnd7qLQUyg2sLasrLjXjmMewFZ9nLfTPGbULyEGVZ05x476tB6L2mOa49xuH0V+5ZO7maeYfo/hVH+EG/aJb7yRUfJEv45GeDXSnuH2XeuepauEc8sD2nStFRFTREVAPqqB91LFql8vpDXF+SiYfoviG9ZUT9p5/wBM1NbK6MJbYPccXCNy5Bl1HGZn3VYcwopuRUucmWoRQVMJbHq20HgoHwockbgB4UXNPnXF/ZUbK0DFAQTSbOOM0DOOdOgsMx4Ulceua5ypIvzppEtgT4UZLfeDQGDuozCOdU2JBWWKHKaKfGgk86QzKorstdXV0HOCEND1dDXUhgdXQ9VXV1ABup76WsbPd/VBPlXV1ALsmcJ0UuN6xjyqcw/RKyolwzHxPzoK6sZSZ0Rgh2mxMIPzE+M0uuxcJH+As8NN3iI1rq6s8maYoBNh2CQBatjvZN3uOlK29j2l3WbJ7+rHu0rq6jJk0h4mzbQ3W7a+CL8qW6tFGgXyUae6urqAClwN1EN3dAJ+dDXUASGC2WGytdv27amJGcFgOUCYNWjAYrBWQAhSZJzBWZu7WNK6uqokyEto9KwjEICTrAIKjUaEjLPl76qWIxly45dzLEjhA05AaCgrqcgSSOHHtwPOkoJ4murqgoVtNlMkK3jPwIowvnXRPs/OhrqAECgNALYrq6gQXKK5kFDXUAErgK6upgJv50juOtdXVUSWHDqN00ZzInhw0/vWurqbBDZ5HGuk11dSA//Z"
                        alt="image logo "
                    />
                     
                </div>
            </div>

            <div className="container text-center mt-5 pt-5 mb-5">
                <h1 className="feature-title">
                    {" "}
                    JDM
                    <span className="feature-title text-success pre-scrollable">
                        Cars
                    </span>
                </h1>
                <div className="d-flex flex-column container flex-wrap flex-md-row justify-content-center gap-5 mt-5 ">
                    <div className="item-card px-4 pb-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://i.pinimg.com/564x/66/2c/42/662c4290ae8ba5c464fb003c757fa77b.jpg"
                            alt=" cabernet Franc "
                        />
                        <h2>Mazda RX-7 (FD)</h2>
                        <p> The JM1FD (FD3S in Japan) was the RX-7's third generation, and while it's arguably the best-looking generation, it's also the most unreliable. However, lots of gearheads were more than willing to overlook its shortcomings in exchange for the intense sensory overload that occurred when driving one. The RX-7 is unique thanks to its twin-turbo 13B-REW Wankel rotary engine, which makes a heavenly sound that may or may not induce nightmares in muscle car lovers.</p>
                    </div>
                    <div className="item-card px-4 pb-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://cdn.jdpower.com/JDPA_2022%20Mercedes%20Benz%20C%20300%20AMG%20Line%20Gray%20Front%20View.jpg"
                            alt=" cabernet Franc "
                        />
                        <h2>Mercedes-Benz C-Class Sedan</h2>
                        <p> the most important cars in the Mercedes-Benz portfolio. The redesigned 2022 Mercedes C-Class sedan will be sold all around the world and produced for those global markets in Germany, China, and South Africa. The compact 5-passenger sedan arrives in the U.S. market in early 2022.</p>
                    </div>
                    <div className="item-card px-4 pb-3 mt-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://static1.hotcarsimages.com/wordpress/wp-content/uploads/2020/10/1989-Nissan-Skyline-R32-GT-R-Cropped.jpg?q=50&fit=crop&w=750&dpr=1.5"
                            alt=" cabernet Franc "
                        />
                        <h2> Nissan Skyline GT-R (R32)</h2>
                        <p> Everyone instantly understands what ‘Godzilla’ refers to in the automotive space, even if they don’t necessarily know how the Nissan R32 Skyline GT-R got its legendary nickname. Injecting a new lease of life into the nameplate in the late '80s, Nissan imbued the Skyline R32 with tech that was ahead of its time, such as the ATTESA E-TS computer-controlled all-wheel drive and HICAS rear-wheel drive systems.</p>
                    </div>
                    <div className="item-card px-4 pb-3 mt-3 col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://cdn.jdpower.com/JDP_2025%20Ford%20Mustang%20GTD%20Front%20Quarter%20View.jpg"
                            alt=" plant image "
                        />
                        <h2> Ford Mustang GTD</h2>
                        <p> Ford is the only remaining American automaker with plans to continue building a gasoline-powered muscle car after 2024, and the company isn’t wasting the opportunity. The 2024 Mustang comes in a new Dark Horse variant that outpowers its GT counterpart. Ford also recently announced the Mustang GTD, a race-ready road car with stunning bodywork, the most powerful engine ever fitted to a Mustang, and loads of lightweight carbon fiber.</p>
                    </div>
                    
                </div>
            </div>

            <div
                className="container section3 d-flex flex-column 
      flex-md-row mt-5 pt-5 justify-content-center align-items-center"
            >
                <div className="image-side col-sm-12 col-md-6 col-lg-5 mt-5 ">
                    <img
                        className="img-fluid" style={{width:"50%"}}
                        src="https://i.pinimg.com/564x/5a/63/8c/5a638c42e41466d40cff7154610b5d70.jpg"
                        alt="image flower"
                    />
                </div>

                <div className="text-side ">
                    <h1 className="feature-title">What Does Car Culture Mean To You?</h1>
                    <ul>
                        <li>I was perched on the bonnet of Saj’s Daimler, with my mates’ RX-8 and 180SX parked either side of my E38. I can’t remember what we were talking about, but there is always a common theme whenever we get together: Food is involved, and it always ends up much later than we’d originally planned.Cars are very much the focus of my social life, as is the case with most of my friends. Yet for all our common interest, we do vary wildly in our approaches to modifying.</li>
                        <li> The variety of style and taste in the car scene is so vast that it’s almost impossible to keep track of it all, even if people do like to paint us with the same brush.

.Speaking from my own experience living in England’s capital, there are so many different aspects of car culture crammed into one city it boggles the mind. I’ve always been a German car fan, and BMWs have been a part of my family since i was little. I’m also mildly (read: highly) obsessed with Japanese VIP-style cars, as well as German tuning from the ’80s and ’90s.</li>
                        
                    </ul>
                </div>
            </div>

            <div
                className="section4 mx-0 mx-lg-5 d-flex flex-column flex-md-row  justify-content-center mt-5       
      align-items-center 
      container"
            >
                <div className="text-side w-100 w-md-75  order-2 order-md-0">
                    <h1 className="feature-title"></h1>
                    <p className="w-100 w-lg-50 pe-0 pe-md-5">
                    That’s why it was only right that – at the age of 19 – I found myself in possession of a BMW 740i Sport. One that just happened to be a single-owner Japanese import with very honourable low miles. This obsession may have slightly influenced Saj over the course of our friendship too, as I managed to convince him it’d be a good idea to purchase a supercharged, long-wheelbase Daimler Super V8.I’ll get into them in more detail in another article though. VIP is one of those styles that hasn’t been explored much in the UK, and both myself and Saj are keen to correct that in the future.
                    
                    </p>

                   
                </div>

                <div className="image-side">
                    <img
                        className="img-fluid rounded-3" style={{width:"80%"}}
                        src="https://s3.amazonaws.com/speedhunters-wp-production/wp-content/uploads/2020/08/24055902/DSC09212-1200x800.jpg"
                        alt=" The Glass riedel"
                    />
                </div>
            </div>
        </div>
    );
};

export default HomePage;
